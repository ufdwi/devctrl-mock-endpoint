# devctrl-mock-endpoint

Mock endpoints for testing devctrl communicators.  A mock endpoint emulates a serial or networked device, 
using a log of captured communications to replay communications and respond to queries.  


Process outline:
   - log all communications (file formats needed for ascii, binary, html)
   - distill logged communications into a "response reservoir" which has duplicate communications removed
   - load response reservoir and launch server at startup 